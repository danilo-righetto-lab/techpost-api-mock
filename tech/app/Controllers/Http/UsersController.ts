import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from "App/Models/User";

export default class UsersController {
  public async create({ request, response }: HttpContextContract) {
    try {
      const { name, email, password } = request.only(["name", "email", "password"]);

      const user = await User.create({
        name,
        email,
        password,
      });

      return user;
    } catch {
      return response.internalServerError('Please, create a table User');
    }
  }

  public async index({ response }: HttpContextContract) {
    try {
      const all = await User.all();
      return all;
    } catch {
      return response.internalServerError('Please, create a table User');
    }
  }

  public async getOne({ params, response }) {
    try {
      const user = await User.findOrFail(params.id);
      return user;
    } catch (error) {
      return response.internalServerError(error);
    }
  }

  public async login({ request, response, auth }: HttpContextContract) {
    const { password, email } = request.only(["email", "password"]);

    const token = await auth.use('api').attempt(email, password, {
      expiresIn: '7days'
    });

    const user = await User.findByOrFail('email', email);
    await auth.use('api').generate(user, {
      expiresIn: '7days'
    })

    return {
      data: token,
      metadata: [],
      status: 200
    }
  }

  public async logout({ response, auth }: HttpContextContract) {
    try {
      if (await auth.use('api').check()) {
        await auth.use('api').revoke();
      }

      return {
        data: [],
        metadata: [],
        status: 200
      }
    } catch (error) {
      return response.internalServerError(error);
    }
  }
}
