import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Post extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public slug: string

  @column()
  public title: string

  @column()
  public subtitle: string

  @column()
  public description: string

  @column()
  public keyword: string

  @column()
  public estimatedReadingTime: string

  @column.dateTime({ autoCreate: true })
  public publishedAt: DateTime

  @column()
  public author: string

  @column()
  public category: string

  @column()
  public tags: string

  @column()
  public featuredImage: string

  @column()
  public url: string

  @column()
  public contentLines: string

  @column()
  public relatedPost: string
}
