import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Posts extends BaseSchema {
  protected tableName = 'posts'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('slug', 255).notNullable()
      table.string('title', 255).notNullable()
      table.string('subtitle', 255).notNullable()
      table.string('description', 255).notNullable()
      table.string('keyword', 255).notNullable()
      table.string('estimatedReadingTime', 255).notNullable()
      table.timestamp('publishedAt', { useTz: true })
      table.string('author', 255)
      table.string('category', 255)
      table.string('tags', 255)
      table.string('featuredImage', 255)
      table.string('url', 255)
      table.string('contentLines', 255)
      table.string('relatedPost', 255)
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
