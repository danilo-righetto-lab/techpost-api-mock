import Factory from '@ioc:Adonis/Lucid/Factory'
import User from 'App/Models/User'
import Post from 'App/Models/Post'
import { DateTime } from 'luxon'

export const UserFactory = Factory
  .define(User, ({ faker }) => {
    return {
      name: faker.internet.userName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
    }
  })
  .build()

export const PostFactory = Factory
  .define(Post, ({ faker }) => {
    return {
      slug: faker.lorem.slug(),
      title: faker.lorem.text(3),
      subtitle: faker.lorem.paragraph(1),
      description: faker.lorem.words(),
      keyword: faker.lorem.sentence(4),
      estimatedReadingTime: '1 min',
      publishedAt: DateTime.local({ zone: "America/New_York" }),
      author: faker.name.findName(),
      category: faker.name.jobArea(),
      tags: faker.name.jobArea(),
      featuredImage: faker.lorem.text(),
      url: faker.internet.url(),
      contentLines: faker.lorem.words(),
      relatedPost: faker.lorem.words(),
    }
  })
  .build()
