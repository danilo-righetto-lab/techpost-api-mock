/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  Route.get('/', async () => {
    return { hello: 'techpost-api' }
  })

  /* Create and List Users */
  Route.get('/users', "UsersController.index")
  Route.get('/users/:id', "UsersController.getOne")
  Route.post('/users', "UsersController.create")

  /* Getting and Invalidating the JWT Token */
  Route.post('/login', "UsersController.login")
  Route.post('/logout', "UsersController.logout")

  /* Using authentication middleware with JWT */
  Route.post('/post/save', "PostsController.save").middleware('auth')
  Route.delete('/post/remove', "PostsController.remove").middleware('auth')
  Route.post('/post/list', "PostsController.list").middleware('auth')
}).prefix('/api/v1');
