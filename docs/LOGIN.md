
# TECHPOST - API - Login

Contrato da API de Login do TECHPOST.

## Objetivo

Login do usuário no TECHPOST.

## URI:
`[POST]`/login

## Corpo da Requisição

```json
{
  "email": "name.lastname@techpost.com.br",
  "password": "123456"
}
```
**Parâmetros necessários na query string:** `Nenhum`

**Parâmetros necessários no cabeçalho:** 
`Content-Type`:`application/json`

## Resposta

```json
{
  "data": {
    "type": "bearer",
    "token": "Mg.Exju7iZwTWq3X33ZFhZNOyQMtN0hZ77TNkI3r7ExNnkXCo6arhmj8-C3jzR1",
    "expires_at": "2021-11-24T11:33:28.207-03:00"
  },
  "metadata": [],
  "status": 200
}
```

**Parâmetros importantes recebidos no corpo:** 
`data`, `metadata`, `status`

**Parâmetros recebidos no cabeçalho:**
`content-length`, `content-type`
