# TECHPOST - API - MOCK

> :rocket: Criando um Mock para o [TECHPOST](https://techpost.com.br/)

## Sobre o projeto

 O projeto foi criado para atender as necessidades da **Sprint 10 e 11** com relação ás API's para que a equipe de **Frontend** possa fazer as integrações sem depender do **Backend**.

## Executando localmente

Para executar o projeto *localmente* siga os passos a seguir:

Entre na pasta `tech`:

    $ cd tech

Instale as dependencias do projeto usando o `npm`:

    $ npm install 

Execute as `migrations`: 

    $ npm run migrate

Execute os `seeders`:

    $ npm run seed

Inicie a aplicação:

    $ npm run dev

Utilize a URL para as suas requisições: `http://localhost:3333/api/v1`

## Documentação - API

A documentação da **API** poderá ser encontrada na pasta `docs` do projeto.